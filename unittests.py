#!/usr/bin/env python3

import unittest
from nif import Nif

class TestNif(unittest.TestCase):
    
    def test_numero (self):
        dni = Nif (51138221, "K")
        self.assertEqual (dni.numeroDNI, 51138221)
    
    def test_letra (self):
        dni = Nif (51138221, "K")
        self.assertEqual(dni.Calcula_letra(),"K")
    
    def test_str(self):
        dni= Nif (51138221, "K")
        self.assertEqual ("La letra del DNI 51138221 es K.", dni.__str__())
    
if __name__ == "__main__":
    unittest.main()


